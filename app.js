"use strict";

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var myGroup = require('./routes/myGroup');
var mySales = require('./routes/mySales');
var investors = require('./routes/investors');
var products = require('./routes/products');
var contest = require('./routes/contest');
var shop = require('./routes/shop');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

var exphbs = require('express-handlebars');
app.engine('.hbs', exphbs({defaultLayout: 'main', extname: '.hbs'}));
app.set('view engine', '.hbs');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'source/public/images', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'source/public')));

console.log("Current Directory --" + path.join(__dirname, 'public'));

//Handle all the routes here

app.use('/', routes);
app.use('/myGroup', myGroup);
app.use('/mySales', mySales);
app.use('/investors', investors);
app.use('/products', products);
app.use('/contest', contest);
app.use('/shop', shop);

app.use("/source/assets", express.static(__dirname + "/source/assets"));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res) {
      res.status(err.status || 500);
      res.render('error', {
          message: err.message,
          error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
